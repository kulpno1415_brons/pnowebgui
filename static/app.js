(function () {
    var app = angular.module('PnO', ['btford.socket-io', 'luegg.directives']);

    app.factory('LegoCar', ['$http', 'socket', function ($http, socket) {
        return {
            move: function (direction) {
                socket.emit("move", direction);
                console.log('move: ' + direction);
            },
            setThrottle: function (amount) {
                socket.emit("throttle", amount);
                console.log('throttle: ' + amount);
            },
            follow_left: function () {
                socket.emit("follow", 'left');
                console.log("follow left");
            },
            follow_right: function () {
                socket.emit("follow", 'right');
                console.log("follow right");
            },
            abort: function () {
                socket.emit("command", {command: "abort"});
                console.log("aborting");
            },
            rotate: function (degrees) {
                socket.emit("command", {command: "rotate", degrees: degrees});
                console.log("rotate :" + degrees)
            },
            nav: function (goal) {
                socket.emit("command", {
                    command: "nav",
                    goal: goal
                })
            },
            nav_coord: function (goal) {
                socket.emit("command", {
                    command: "nav_coord",
                    goal: goal
                })
            },
            shape_hunt: function () {
                socket.emit("command", {
                    command: "hunt"
                })
            },
            reset: function (start, orientation) {
                socket.emit("command", {
                    command: "reset",
                    position: start,
                    orientation: orientation
                })
            }

        };
    }]);


    app.factory('socket', ['socketFactory', '$location', function (socketFactory, $location) {
        return socketFactory({
            ioSocket: io.connect($location.host() + ':' + $location.port())
        });
    }]);

    app.controller('RobotController', ['$scope', 'LegoCar', 'socket', 'DebugMessages', function ($scope, LegoCar, socket, DebugMessages) {
        $scope.direction = "";
        $scope.car = LegoCar;
        $scope.sonar_lego = 0;
        $scope.cars= [];
        $scope.sonar_pcb = 0;
        $scope.throttle = 100;
        $scope.last_key = 0;
        $scope.map = {};
        $scope.carStyle = {};
        $scope.targetStyle = {display: 'none'};
        $scope.shape_last_updated = 0;
        $scope.shape_name = "";
        $scope.last_map_data = {};
        $scope.scaleStyle = {
            transform: 'scale(1,1)'
        };
        $scope.scale = 1;
        $scope.setCarStyle = function (car) {
            car.carStyle = {
                //transform: 'translate('+ x*100 + 'px,' + y*100 + 'px) rotate(' + Math.floor(rot) + 'deg)'
                top: car.y * 100 - 7 + 'px',
                left: car.x * 100 - 18 + 'px',
                transform: 'rotate(' + Math.floor(car.orientation) + 'deg)'
            };
        };

        $scope.parsePath = function (path) {
            //console.log(path);
            var result = [];
            var j = 0;
            $scope.side_points = path.side_points;
            $scope.front_points = path.front_points;
            $scope.cars = [];
            $scope.shape_last_updated = path.image_last_modified;
            $scope.shape_name = path.shape_found;
            for (var i = 0; i < path.cars.length; i++) {
                var temp = path.cars[i];
                $scope.setCarStyle(temp);
                $scope.cars.push(temp);
            }
            //console.log($scope.cars);
            if (path.target.length > 0) {
                $scope.targetStyle = {
                    position: 'absolute',
                    top: path.target[1] * 100 + 'px',
                    left: path.target[0] * 100 + 'px'
                }
            } else {
                $scope.targetStyle = {display: 'none'};
            }
            for (i = 0; i < path.path.length; i++) {
                if (j >= path.actions.length) {
                    break;
                }
                while (path.actions[j].type == "turn") {
                    j++;
                }
                var currentPos = path.path[i];
                result.push({x: currentPos[0], y: currentPos[1], direction: path.actions[j]["direction"]});
                j++;
            }
            $scope.path = result;
        };

        $scope.$watch('throttle', $scope.car.setThrottle);
        socket.on("sonarLego", function (response) {
            $scope.sonar_lego = response;
        });
        socket.on("sonarPCB", function (response) {
            $scope.sonar_pcb = response;
        });
        socket.on("map", function (response) {
            //console.log(response);
            if($scope.last_map_data != angular.toJson(response)) {
                console.log("updated map");
                $scope.map = response;
                $scope.last_map_data = angular.toJson(response);
            }
        });

        socket.on("path", function (response) {
            //console.log(response);
            $scope.parsePath(response);
        });

        $scope.hasWall = function (block, direction) {
            return (block.walls.indexOf(direction) != -1);
        };
        $scope.hasImage = function (block, direction) {
            return (block.image_orientation == direction);
        };

        $scope.keypress = function ($event) {
            DebugMessages.log('key:' + $event.which, 'local');
            var key_num = $event.which;
            switch (key_num) {
                case 37: // left arrow: FORWARD LEFT
                    LegoCar.move("forward_left");
                    $scope.direction = "forward_left";
                    $scope.last_key = key_num;
                    break;
                case 38: // up arrow: FORWARD
                    LegoCar.move("forward");
                    $scope.direction = "forward";
                    $scope.last_key = key_num;

                    break;
                case 39: // right arrow: FORWARD RIGHT
                    LegoCar.move("forward_right");
                    $scope.direction = "forward_right";
                    $scope.last_key = key_num;

                    break;
                case 73: // I: ROTATE LEFT
                    LegoCar.move("rotate_left");
                    $scope.direction = "rotate_left";
                    $scope.last_key = key_num;

                    break;
                case 79: // O: ROTATE RIGHT
                    LegoCar.move("rotate_right");
                    $scope.direction = "rotate_right";
                    $scope.last_key = key_num;

                    break;
                case 75: // K: BACKWARD LEFT
                    LegoCar.move("backward_left");
                    $scope.direction = "backward_left";
                    $scope.last_key = key_num;

                    break;
                case 40: // down arrow: BACKWARD
                    LegoCar.move("backward");
                    $scope.direction = "backward";
                    $scope.last_key = key_num;

                    break;
                case 76: // L: BACKWARD RIGHT
                    LegoCar.move("backward_right");
                    $scope.direction = "backward_right";

                    $scope.last_key = key_num;
                    break;
                case 17: // control: DECREASE THROTTLE
                    $scope.throttle -= 5;
                    $scope.down = false;
                    if ($scope.throttle < 0) {
                        $scope.throttle = 0
                    }
                    break;
                case 16: // shift: INCREASE THROTTLE
                    $scope.throttle += 5;
                    $scope.down = false;
                    if ($scope.throttle > 100) {
                        $scope.throttle = 100
                    }
                    break;
                default :
                    $scope.down = false;
                    break;
            }
        };
        $scope.keyup = function ($event) {
            var action_keys = [37, 38, 39, 40, 73, 79, 75, 76];
            if (action_keys.indexOf($event.which) != -1 && $event.which == $scope.last_key) {
                LegoCar.move('stop');
                $scope.down = false;
                $scope.direction = "";
            }
        };

        $scope.follow_left = LegoCar.follow_left;

        $scope.follow_right = LegoCar.follow_right;

        $scope.nav = LegoCar.nav;
        $scope.nav_coord = LegoCar.nav_coord;
        $scope.reset = LegoCar.reset;
        $scope.abort = LegoCar.abort;
        $scope.rotate = LegoCar.rotate;
        $scope.shape_hunt = LegoCar.shape_hunt;

        $scope.$watch('scale',function(newScale){
            $scope.scaleStyle = {
                transform: 'scale(' + newScale + ',' + newScale + ')'
            };
        })

    }]);

    app.controller('DebugController', ['$scope', 'socket', 'DebugMessages', function ($scope, socket, DebugMessages) {
        $scope.glued1 = true;
        $scope.glued2 = true;
        $scope.glued3 = true;
        $scope.messages = DebugMessages;
        $scope.debugOpen = false;
        socket.on("debug-node", function (response) {
            DebugMessages.log(response, 'node');
        });
        socket.on("debug-python", function (response) {
            DebugMessages.log(response.log, 'python');
            console.log(response);
        });
    }]);

    app.factory('DebugMessages', function () {
        return {
            nodeMessages: [],
            pythonMessages: [],
            localMessages: [],
            log: function (message, list) {
                var msgList = [];
                switch (list) {
                    case 'node':
                        msgList = this.nodeMessages;
                        break;
                    case 'python':
                        msgList = this.pythonMessages;
                        break;
                    case 'local':
                        msgList = this.localMessages;
                        break;
                    default :
                        break;
                }
                if (angular.isArray(message)) {
                    for (var i = 0; i < message.length; i++) {
                        msgList.push({time: Date.now(), data: message[i]});
                    }
                } else {
                    msgList.push({time: Date.now(), data: message});

                }
            },
            clear: function (list) {
                switch (list) {
                    case 'node':
                        this.nodeMessages = [];
                        break;
                    case 'python':
                        this.pythonMessages = [];
                        break;
                    case 'local':
                        this.localMessages = [];
                        break;
                    case 'all':
                        this.nodeMessages = [];
                        this.pythonMessages = [];
                        this.localMessages = [];
                        break;
                    default :
                        break;
                }
            }
        }
    });

    app.directive('backImg', function(){
        return function(scope, element, attrs){
            var url = attrs.backImg;
            if(url != null) {
                element.css({
                    'background-image': 'url(' + url + ')',
                    'background-size': '40px 40px',
                    'background-repeat': 'no-repeat'
                });
            }
        };
    });
})();
