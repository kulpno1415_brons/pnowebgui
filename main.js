var http = require('http');
var util = require('util');
var express = require('express');
var Netcat = require('node-netcat');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var DCHost = 'brons.local';
app.use(express.static(__dirname + '/static'));
app.use(express.static('/img'));

var socketClient = {
    client: undefined,
    getOrNew: function () {
        if (this.client == undefined) {
            this.client = Netcat.client(5000, DCHost, {timeout: 0});
            var self = this;

            this.client.on('close', function () {
                self.client = undefined;
                log('SOCKET DISCONNECTED');
            });
            this.client.on('data', dataHandler);
            this.client.on('error', function (err) {
                io.sockets.emit('debug-python', err.message);
            });
            this.client.start();
            log('SOCKET CONNECTING');
            startData();
        }
        return this.client;
    }
};

var dataHandler = function (res) {
    var regex = /^\|({[^\|]+})\|(.*)/;
    var response = res.toString('ascii');
    var match = response.match(regex);
    while (match != null) {
        var command = JSON.parse(match[1]);
        console.log('data:' + JSON.stringify(command));
        if (command.return == 'data') {
            if (command.sonarLego) {
                io.sockets.emit('sonarLego', command.sonarLego);
            }
            if (command.sonarPCB) {
                io.sockets.emit('sonarPCB', command.sonarPCB);
            }
            if (command.map) {
                io.sockets.emit('map', command.map);
                console.log('got map');
            }
            if (command.path && command.actions) {
                io.sockets.emit('path',
                    command);

            }
            if (util.isArray(command.log)) {
                io.sockets.emit('debug-python', command);
            }
        }
        match = match[2].match(regex);
    }
};

io.on('connection', function (socket) {
    log('socket.io connected');

    // GUI TO ROBOT
    socket.on('throttle', function (data) {
        //log('throttle: ' + data);
        sendCommand({command: 'throttle', value: data});
    });

    // MOVE COMMANDS
    socket.on('move', function (data) {
        sendCommand({command: 'move', direction: data});
        //log(data);
    });

    socket.on('command', function (data) {
        sendCommand(data);
        //console.log('sending:' + JSON.stringify(data));
    });

    socket.on('follow', function (data) {
        if (data == 'left') {
            sendCommand({command: 'follow', direction: 'left'});
        } else {
            sendCommand({command: 'follow', direction: 'right'});
        }
    });
    startData();

});

var startData = function () {
    sendCommand({
        command: 'data',
        type: 'sensor'
    });
    sendCommand({
        command: 'data',
        type: 'log'
    });
    sendCommand({
        command: 'data',
        type: 'map'
    });
};

var sendCommand = function (command) {
    var client = socketClient.getOrNew();
    client.send('|' + JSON.stringify(command) + '|');
};

var log = function (message) {
    console.log(message);
    io.sockets.emit('debug-node', message);
};

server.listen(1337);
log('Server running at http://127.0.0.1:1337/');